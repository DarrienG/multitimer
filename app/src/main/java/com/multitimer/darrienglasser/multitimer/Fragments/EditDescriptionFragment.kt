package com.multitimer.darrienglasser.multitimer.Fragments

import android.app.ActionBar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.multitimer.darrienglasser.multitimer.R
import kotlinx.android.synthetic.main.fragment_edit_description.*



private const val TASK_DESCRIPTION = "taskDescription"

class EditDescriptionFragment : DialogFragment() {
    private var taskDescription: String? = null
    var choiceListener: ((Boolean, String) -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            taskDescription = it.getString(TASK_DESCRIPTION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_description, container, true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundledText = arguments?.getString(TASK_DESCRIPTION)
        val descriptionText =  if (bundledText.isNullOrEmpty()) "" else bundledText
        edit_description.setText(descriptionText)

        val title = context?.getString(R.string.edit_description_text)
        title_text.text = title

        update_button.setOnClickListener {
            choiceListener?.invoke(true, edit_description.text.toString())
        }

        cancel_button.setOnClickListener {
            choiceListener?.invoke(false, edit_description.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        val window = dialog!!.window ?: return
        val params = window.attributes
        params.width = ActionBar.LayoutParams.MATCH_PARENT
        params.height = ActionBar.LayoutParams.WRAP_CONTENT
        window.attributes = params
    }

    companion object {
        @JvmStatic
        fun newInstance(taskDescription: String) =
            EditDescriptionFragment().apply {
                arguments = Bundle().apply {
                    putString(TASK_DESCRIPTION, taskDescription)
                }
            }
    }
}
