package com.multitimer.darrienglasser.multitimer.Database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.multitimer.darrienglasser.multitimer.Dao.TimeEntityDao
import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity

@Database(entities = [TimeEntity::class], version = 1)
abstract class TimeDb : RoomDatabase() {
    abstract fun timeDao(): TimeEntityDao
}



