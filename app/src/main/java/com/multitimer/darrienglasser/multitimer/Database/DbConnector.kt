package com.multitimer.darrienglasser.multitimer.Database

import android.content.Context
import androidx.room.Room

class DbConnector(context: Context) {
    companion object {
        @JvmStatic
        private var _db: TimeDb? = null

        @JvmStatic
        public var db: TimeDb
            get() {
                while (_db == null) {
                    Thread.sleep(100)
                }
                return _db!!
            }
            set(value) { _db = value }
    }

    init {
        _db = Room.databaseBuilder(
            context,
            TimeDb::class.java, "TimeEntity"
        ).build()
    }
}

