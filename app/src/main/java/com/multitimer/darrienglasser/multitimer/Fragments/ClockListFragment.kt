package com.multitimer.darrienglasser.multitimer.Fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.UiThread
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.multitimer.darrienglasser.multitimer.Adapter.ClockListAdapter
import com.multitimer.darrienglasser.multitimer.Database.DbConnector
import com.multitimer.darrienglasser.multitimer.Entity.TimeEntity
import com.multitimer.darrienglasser.multitimer.Events.NewTimerEvent
import com.multitimer.darrienglasser.multitimer.Events.TimerCompletedEvent
import com.multitimer.darrienglasser.multitimer.R
import kotlinx.android.synthetic.main.fragment_clock_list.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ClockListFragment : PageFragment() {
    override fun getTitle(): String = "Timers"
    private lateinit var adapter: ClockListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_clock_list, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkListAndSetup()
        new_clock.setOnClickListener {
            EventBus.getDefault().post(NewTimerEvent())
            GlobalScope.launch {
                val id = DbConnector.db.timeDao().newTime(
                    TimeEntity(
                        null,
                        0,
                        "",
                        (System.currentTimeMillis() / 1000L),
                        -1,
                        true,
                        false
                    )
                )
                if (!::adapter.isInitialized) { checkListAndSetup(); return@launch }
                Handler(Looper.getMainLooper()).post {
                    adapter.addTimer(id.toInt())
                }
            }
        }
    }

    private fun checkListAndSetup() {
        GlobalScope.launch {
            val ids = DbConnector.db.timeDao().getActiveId()
            setUpClockList(ids)
        }
    }

    @UiThread
    fun setUpClockList(ids: List<Int>) {
        Handler(Looper.getMainLooper()).post {
            adapter = ClockListAdapter(ids.toMutableList())

            clock_list_views.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0 || dy < 0 && new_clock.isShown)
                        new_clock.hide()
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        new_clock.show()
                    }
                    if (!recyclerView.canScrollVertically(1)) {
                        new_clock.hide()
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })

            clock_list_views.layoutManager = LinearLayoutManager(context)
            clock_list_views.adapter = adapter
            if (ids.isEmpty()) add_me_view.visibility = View.VISIBLE
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun removeTimer(timerCompletedEvent: TimerCompletedEvent) {
        if (::adapter.isInitialized) {
            adapter.remove(timerCompletedEvent.entity.id)
            if (adapter.itemCount == 0) add_me_view.visibility = View.VISIBLE
        }
        new_clock.visibility = View.VISIBLE
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun timerAdded(newTimerEvent: NewTimerEvent) {
        if (adapter.itemCount > 9) new_clock.visibility = View.GONE

        add_me_view.visibility = View.GONE
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }
}
